package app;

import app.forest.ForestFly;
import app.forest.ForestNormal;

import java.awt.*;

public class Main {
    static int CANVAS_SIZE = 500;
    static int TREES_TO_DRAW = 1000000;
    static int TREE_TYPES = 2;

    public static void main(String[] args) {
        int mb = 1024*1024;

        //Normal
        long beforeUsedMem=Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();

        ForestNormal forest = new ForestNormal();
        for (int i = 0; i < Math.floor(TREES_TO_DRAW / TREE_TYPES); i++) {
            forest.plantTree(random(0, CANVAS_SIZE), random(0, CANVAS_SIZE),
                    "Summer Oak", Color.GREEN, "Oak texture stub");
            forest.plantTree(random(0, CANVAS_SIZE), random(0, CANVAS_SIZE),
                    "Autumn Oak", Color.ORANGE, "Autumn Oak texture stub");
        }
        forest.setSize(CANVAS_SIZE, CANVAS_SIZE);
        forest.setVisible(true);

        long afterUsedMem=Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();

        long actualMemUsed=afterUsedMem-beforeUsedMem;

        //Use Flyweight
        long beforeUsedMemF=Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();

        ForestFly forest1 = new ForestFly();
        for (int i = 0; i < Math.floor(TREES_TO_DRAW / TREE_TYPES); i++) {
            forest1.plantTree(random(0, CANVAS_SIZE), random(0, CANVAS_SIZE),
                    "Summer Oak", Color.GREEN, "Oak texture stub");
            forest1.plantTree(random(0, CANVAS_SIZE), random(0, CANVAS_SIZE),
                    "Autumn Oak", Color.ORANGE, "Autumn Oak texture stub");
        }
        forest1.setSize(CANVAS_SIZE, CANVAS_SIZE);
        forest1.setVisible(true);

        long afterUsedMemF=Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();

        long actualMemUsedF=afterUsedMemF-beforeUsedMemF;

        System.out.println("Normal Used Memory: "+(actualMemUsed / mb)+" MB");
        System.out.println("Flyweight Used Memory: "+(actualMemUsedF / mb)+" MB");


    }

    private static int random(int min, int max) {
        return min + (int) (Math.random() * ((max - min) + 1));
    }
}
